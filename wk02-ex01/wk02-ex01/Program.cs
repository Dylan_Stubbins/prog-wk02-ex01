﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk02_ex01
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 3;
            var b = 5;
            if (a == b) 
            {
                Console.WriteLine("These values are equal");
            }
            else
            {
                Console.WriteLine("These values are not equal");
            }

        }
    }
}
